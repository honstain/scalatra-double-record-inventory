package org.bitbucket.honstain.inventory.dao

import org.slf4j.{Logger, LoggerFactory}
import slick.jdbc.{GetResult, PostgresProfile, TransactionIsolation}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

case class InventorySingleRecord(
                                  id: Option[Int],
                                  sku: String,
                                  qty: Int,
                                  location: String
                                )

class InventorySingleRecords(tag: Tag) extends Table[InventorySingleRecord](tag, "inventory_single") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def sku = column[String]("sku")
  def qty = column[Int]("qty")
  def location = column[String]("location")
  def * =
    (id.?, sku, qty, location) <> (InventorySingleRecord.tupled, InventorySingleRecord.unapply)
}

object InventorySingleRecordDao extends TableQuery(new InventorySingleRecords(_)) {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  implicit val getInventorySingleRecord : GetResult[InventorySingleRecord] =
    GetResult(r => InventorySingleRecord(r.<<, r.<<, r.<<, r.<<))

  def findAll(db: PostgresProfile.backend.DatabaseDef): Future[Seq[InventorySingleRecord]] = {
    db.run(this.result)
  }

  def findBySku(db: PostgresProfile.backend.DatabaseDef, sku: String): Future[Seq[InventorySingleRecord]] = {
    val findAction = this.filter(_.sku === sku)
    db.run(findAction.result)
  }

  def create(db: PostgresProfile.backend.DatabaseDef,
             sku: String,
             qty: Int,
             location: String
            ): Future[Option[InventorySingleRecord]] = {
    val upsert = for {
      existing <- {
        this.filter(x => x.location === location && x.sku === sku).forUpdate.result.headOption
      }
      _ <- {
        existing match {
          case Some(InventorySingleRecord(_, `sku`, _, `location`)) => // Update
            val q = for {x <- this if x.location === location && x.sku === sku} yield x.qty
            q.update(qty)
          case _ => // Create a new record
            TableQuery[InventorySingleRecords] += InventorySingleRecord(Option.empty, sku, qty, location)
        }
      }
      updated <- {
        TableQuery[InventorySingleRecords].filter(x => x.location === location && x.sku === sku).result.headOption
      }
    } yield updated
    db.run(upsert.transactionally)
  }

    def transfer(db: PostgresProfile.backend.DatabaseDef,
                 sku: String,
                 qty: Int,
                 fromLocation: String,
                 toLocation: String
                ): Future[Int] = {

      val (firstLockLocation, secondLockLocation): (String, String) = {
        if (fromLocation < toLocation)
          (fromLocation, toLocation)
        else
          (toLocation, fromLocation)
      }

      val insert = for {
        _ <- {
          this.filter(x => x.location === firstLockLocation && x.sku === sku).forUpdate.result
        }
        _ <- {
          this.filter(x => x.location === secondLockLocation && x.sku === sku).forUpdate.result
        }
        toRecord <- {
          this.filter(x => x.location === toLocation && x.sku === sku).result.headOption
        }
        fromRecord <- {
          this.filter(x => x.location === fromLocation && x.sku === sku).result.headOption
        }
        createUpdateDestination <- {
          toRecord match {
            case Some(InventorySingleRecord(_, `sku`, destQty, `toLocation`)) =>
              // Update
              logger.debug(s"Transfer from:$fromLocation to $toLocation found $destQty in destination")
              val q = for { x <- this if x.location === toLocation && x.sku === sku } yield x.qty
              q.update(destQty + qty)
            case _ =>
              // Create - this is likely susceptible to write skew
              this += InventorySingleRecord(Option.empty, sku, qty, toLocation)
          }
        }
        updateSource <- {
          fromRecord match {
            case Some(InventorySingleRecord(_, `sku`, srcQty, `fromLocation`)) if srcQty >= qty =>
              logger.debug(s"Transfer from:$fromLocation to $toLocation found $srcQty in source")
              val q = for { x <- this if x.location === fromLocation && x.sku === sku } yield x.qty
              q.update(srcQty - qty)
            case Some(InventorySingleRecord(_, `sku`, srcQty, `fromLocation`)) if srcQty < qty || srcQty <= 0 =>
              logger.error(s"Transfer from:$fromLocation to $toLocation found $srcQty in source, needed $qty")
              DBIO.failed(new Exception("INSUFFICIENT INVENTORY"))
            case  _ =>
              DBIO.failed(new Exception("FAILED"))
          }
        }
      } yield updateSource
      db.run(insert.transactionally)
    }
}
