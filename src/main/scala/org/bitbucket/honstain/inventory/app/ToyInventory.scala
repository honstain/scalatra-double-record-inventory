package org.bitbucket.honstain.inventory.app

import org.bitbucket.honstain.inventory.dao._
import org.scalatra._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
// JSON handling support from Scalatra
import org.scalatra.json._

import slick.jdbc.PostgresProfile

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

class ToyInventory(database: PostgresProfile.backend.DatabaseDef) extends ScalatraServlet with JacksonJsonSupport {

  val logger: Logger = LoggerFactory.getLogger(getClass)
  protected implicit val jsonFormats: Formats = DefaultFormats

  val doubleDAO = InventoryDoubleRecordDao
  val singleDAO = InventorySingleRecordDao

  before() {
    contentType = formats("json")
  }

  get("/single") {
    val futureResult = Await.result(singleDAO.findAll(database), Duration.Inf)
    logger.debug(s"GET retrieved ${futureResult.size} inventory records")
    Ok(futureResult.map(x => Inventory(x.sku, x.qty, x.location)))
  }

  get("/double") {
    val futureResult = Await.result(doubleDAO.findAll(database), Duration.Inf)
    //logger.debug(s"GET retrieved ${futureResult.size} inventory records")
    val head = futureResult.headOption
    head match {
      case Some((sku, loc, qty)) =>
        logger.debug(s"GET: location: $loc sku: $sku qty: $qty")
      case _ => None
    }
    Ok(futureResult.map { case (sku, loc, qty) => Inventory(sku, qty.getOrElse(0), loc) })
  }

  post("/single") {
    val newInventory = parsedBody.extract[Inventory]
    logger.debug(s"Creating inventory sku:${newInventory.sku} in location:${newInventory.location}")

    val future: Future[Option[InventorySingleRecord]] = singleDAO.create(database, newInventory.sku, newInventory.qty, newInventory.location)
    future.onComplete {
      case Success(Some(value)) => logger.debug(s"Created new inventory record id:${value.id}")
      case Success(None) =>
        // Does it even make sense to try and account case like this?
        logger.error(s"Failed to create or update sku:${newInventory.sku} in location:${newInventory.location}")
      case Failure(t) => logger.error(t.getMessage)
    }
    val result = Await.result(future, Duration.Inf)
    if (result.isDefined) {
      logger.debug(s"Created new inventory record id:${result.get}")
      // TODO - decide on more appropriate response data
      Ok
    }
    else {
      InternalServerError
    }
  }

  post("/double") {
    val newInventory = parsedBody.extract[Inventory]
    val result: Future[InventoryDoubleRecord] = doubleDAO.create(database, newInventory.location, newInventory.sku, newInventory.qty)
    result.onComplete {
      case Success(_) => None //logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug("FAIL " + t.getMessage)
    }
    val futureResult = Await.result(result, Duration.Inf)

    Ok(Inventory(futureResult.sku, futureResult.qty, futureResult.location))
  }

  post("/single/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    logger.debug(s"Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val future: Future[Int] = singleDAO.transfer(database, transfer.sku, transfer.qty, transfer.fromLocation, transfer.toLocation)
    future.onComplete {
      case Success(_) => logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug(t.getMessage)
    }
    Await.result(future, Duration.Inf)
    Ok
  }

  post("/double/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    logger.debug(s"Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val future: Future[Int] = doubleDAO.transfer(database, transfer.sku, transfer.qty, transfer.fromLocation, transfer.toLocation)
    future.onComplete {
      case Success(_) => logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug(t.getMessage)
    }
    Await.result(future, Duration.Inf)
    Ok
  }
}

case class Inventory(sku: String, qty: Int, location: String)
case class InventoryTransfer(sku: String, qty: Int, fromLocation: String, toLocation: String)