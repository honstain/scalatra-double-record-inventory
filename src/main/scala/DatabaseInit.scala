import org.slf4j.LoggerFactory
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

trait DatabaseInit {
  val logger = LoggerFactory.getLogger(getClass)

  def database : PostgresProfile.backend.DatabaseDef = {
    Database.forConfig("mydb")
  }
}
