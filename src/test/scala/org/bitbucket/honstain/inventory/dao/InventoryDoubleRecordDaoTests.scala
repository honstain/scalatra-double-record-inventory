package org.bitbucket.honstain.inventory.dao

import org.bitbucket.honstain.PostgresSpec

import org.scalatest.BeforeAndAfter
import org.scalatra.test.scalatest._
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class InventoryDoubleRecordDaoTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  def createInventoryTable: DBIO[Int] =
    sqlu"""
          CREATE TABLE inventory_double
          (
            id bigserial NOT NULL,
            sku text,
            qty integer,
            type text,
            location text,
            CONSTRAINT pk_double PRIMARY KEY (id)
          );

          CREATE TABLE inventory_lock
          (
            location text,
            sku text,
            revision integer,
            CONSTRAINT pk_lock PRIMARY KEY (location, sku)
          );
      """
  def dropInventoryTable: DBIO[Int] =
    sqlu"""
          DROP TABLE IF EXISTS inventory_double;
          DROP TABLE IF EXISTS inventory_lock;
      """

  def createInventoryHelper(sku: String, location: String, qty: Int = 1): InventoryDoubleRecord = {
    val create = InventoryDoubleRecordDao.create(database, location, sku, qty)
    Await.result(create, Duration.Inf)
  }

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  val TEST_SKU = "NewSku"
  val BIN_01 = "Bin-01"
  val BIN_02 = "Bin-02"

  test("findAll when empty") {
    val futureFind = InventoryDoubleRecordDao.findAll(database)
    val findResult: Seq[(String, String, Option[Int])] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List())
  }

  test("create when empty") {
    val futureCreate = InventoryDoubleRecordDao.create(database, BIN_01, TEST_SKU, 2)
    val createResult: InventoryDoubleRecord = Await.result(futureCreate, Duration.Inf)
    createResult should equal(InventoryDoubleRecord(None, TEST_SKU, 2, TRANSACTION.ADJUST, BIN_01))

    val futureFind = InventoryDoubleRecordDao.findAllRaw(database)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List(InventoryDoubleRecord(Option(1), TEST_SKU, 2, TRANSACTION.ADJUST, BIN_01)))
  }

  test("create with existing record") {
    val futureCreate = InventoryDoubleRecordDao.create(database, BIN_01, TEST_SKU, 2)
    val createResult: InventoryDoubleRecord = Await.result(futureCreate, Duration.Inf)

    val futureUpdate = InventoryDoubleRecordDao.create(database, BIN_01, TEST_SKU, 1)
    val updateResult: InventoryDoubleRecord = Await.result(futureUpdate, Duration.Inf)
    updateResult should equal(InventoryDoubleRecord(None, TEST_SKU, 1, TRANSACTION.ADJUST, BIN_01))

    val futureFind = InventoryDoubleRecordDao.findAllRaw(database)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List(
      InventoryDoubleRecord(Option(1), TEST_SKU, 2, TRANSACTION.ADJUST, BIN_01),
      InventoryDoubleRecord(Option(2), TEST_SKU, -1, TRANSACTION.ADJUST, BIN_01),
    ))
  }

  test("transfer") {
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureTrans = InventoryDoubleRecordDao.transfer(database, TEST_SKU, 1, BIN_01, BIN_02)
    val transResult = Await.result(futureTrans, Duration.Inf)

    val futureFind = InventoryDoubleRecordDao.findBySkuRaw(database, TEST_SKU)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List(
      InventoryDoubleRecord(Some(1), "NewSku", 1, TRANSACTION.ADJUST, BIN_01),
      InventoryDoubleRecord(Some(2), "NewSku", -1, TRANSACTION.TRANSFER, BIN_01),
      InventoryDoubleRecord(Some(3), "NewSku", 1, TRANSACTION.TRANSFER, BIN_02),
    ))
  }

  test("transfer fail for insufficient funds") {
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureTrans = InventoryDoubleRecordDao.transfer(database, TEST_SKU, 2, BIN_01, BIN_02)
    val error = assertThrows[Exception](Await.result(futureTrans, Duration.Inf))

    val futureFind = InventoryDoubleRecordDao.findBySkuRaw(database, TEST_SKU)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List(
      InventoryDoubleRecord(Some(1), "NewSku", 1, TRANSACTION.ADJUST, BIN_01),
    ))
  }

  test("findQty") {
    createInventoryHelper(TEST_SKU, BIN_01)

    val qty: Option[Int] = Await.result(database.run(
      InventoryDoubleRecordDao.findQty(TEST_SKU, BIN_01).result), Duration.Inf)
    qty should equal(Option(1))

    val insertRaw = TableQuery[InventoryDoubleRecords] ++= Seq(
      InventoryDoubleRecord(None, TEST_SKU, 2, TRANSACTION.TRANSFER, BIN_02),
      InventoryDoubleRecord(None, TEST_SKU, 3, TRANSACTION.TRANSFER, BIN_01),
      InventoryDoubleRecord(None, TEST_SKU, -1, TRANSACTION.TRANSFER, BIN_01),
    )
    Await.result(database.run(insertRaw), Duration.Inf)

    val qtyMultiple: Option[Int] = Await.result(database.run(
      InventoryDoubleRecordDao.findQty(TEST_SKU, BIN_01).result), Duration.Inf)
    qtyMultiple should equal(Option(3))
  }
}